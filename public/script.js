let quote = document.querySelector('#text');
let author = document.querySelector('#author');
let btn = document.querySelector('#new-quote');
let tweet = document.querySelector('#tweet-quote');
let tumbler = document.querySelector('#tumbler-quote');

let currentQuote = "";
let currentAuthor = "";

const uri = "https://gist.githubusercontent.com/camperbot/5a022b72e96c4c9585c32bf6a75f62d9/raw/e3c6895ce42069f0ee7e991229064f167fe8ccdc/quotes.json"
let quotes = [];
let randomNumber = Math.floor(Math.random() * 102)
let randomColor = Math.floor(Math.random() * 12)


var colors = [
  '#16a085',
  '#27ae60',
  '#2c3e50',
  '#f39c12',
  '#e74c3c',
  '#9b59b6',
  '#FB6964',
  '#342224',
  '#472E32',
  '#BDBB99',
  '#77B1A9',
  '#73A857'
];

let color = colors[randomColor]

const fetchData = async () => {
	let response = await fetch(uri);
	let data = await response.json();
	return data.quotes;
}

fetchData()
.then(data => {
	quotes = [...data]
})
.then(() => {
	console.log(quotes);
	getQuote(quotes)
	document.body.style.backgroundColor = color;
	btn.style.backgroundColor = color;
	tweet.style.backgroundColor = color;
	tumbler.style.backgroundColor = color;
});



function getQuote(data){
	
	let quoteFromApi = data[randomNumber];
	
	quote.innerText = quoteFromApi.quote;
	author.innerText = "- " + quoteFromApi.author;

	currentQuote = quoteFromApi.quote;
	currentAuthor = quoteFromApi.author;


}

// when button is pressed changes quote inside.
function getNewQuote(){
	let newRandomNumber = Math.floor(Math.random() * 102);
	
	if(randomNumber !== newRandomNumber){
		quote.innerText = quotes[newRandomNumber].quote;
		author.innerText = quotes[newRandomNumber].author;
	} else {
		quote.innerText = quotes[newRandomNumber + 1].quote;
		author.innerText = quotes[newRandomNumber + 1].author;
	}
	
	color = colors[Math.floor(Math.random() * 12)]

	document.body.style.backgroundColor = color;
	btn.style.backgroundColor = color;
	tweet.style.backgroundColor = color;
	tumbler.style.backgroundColor = color;
}

btn.addEventListener('click', () => {
	getNewQuote();
})

tweet.addEventListener('click', () => {
	console.log('tweet')

	tweet.setAttribute('href', 'https://twitter.com/intent/tweet?hashtags=quotes&related=freecodecamp&text=' + currentQuote + " - " + currentAuthor);
})




